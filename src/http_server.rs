use crate::errors::IqiperOIDCCliError;
use actix_web::{dev::Server, get, web, App, HttpResponse, HttpServer, Responder};
use log::debug;
use serde::Deserialize;
use std::sync::mpsc;

#[derive(Debug, Deserialize)]
pub struct AuthorizationCodeFlowParameters {
    /// The session state joined with the authorization code.
    /// This state will be sent back from the IdP
    pub state: Option<String>,
    /// The authorization code issued by the IdP. This token can be exchanged
    /// for ID, Access and Refresh tokens
    pub code: String,
}

const CLOSE_PAGE_HTML: &'static str = "<html><head><title>Will close</title></head><script type=\"text/javascript\">self.close();</script><body></body></html>";

#[get("/auth_code")]
async fn authorization_code_sink_hole(
    query: web::Query<AuthorizationCodeFlowParameters>,
    tx_sd: web::Data<mpsc::Sender<AuthorizationCodeFlowParameters>>,
) -> impl Responder {
    debug!("Received query, responding closing html page");
    let res = HttpResponse::Ok()
        .set_header(
            actix_http::http::header::CONTENT_TYPE,
            "text/html; charset=utf-8",
        )
        .body(CLOSE_PAGE_HTML);
    debug!("Sending auth code to main thread");
    tx_sd
        .into_inner()
        .send(query.into_inner())
        .expect("The shutdown command should've arrived to the server");
    res
}

pub fn run_web_server(
    tx_srv: mpsc::Sender<Server>,
    tx_sd: mpsc::Sender<AuthorizationCodeFlowParameters>,
    host: String,
    port: String,
) -> Result<(), IqiperOIDCCliError> {
    let mut sys = actix_rt::System::new("test");

    let server = HttpServer::new(move || {
        App::new()
            .data(tx_sd.clone())
            .service(authorization_code_sink_hole)
    })
    .bind(format!("{}:{}", host, port))?
    .workers(1)
    .shutdown_timeout(1)
    .run();
    debug!("Sending server handle to main thread");
    let _ = tx_srv.send(server.clone());
    Ok(sys.block_on(server)?)
}
