// // (Full example with detailed comments in examples/01b_quick_example.rs)
// //
// // This example demonstrates clap's full 'builder pattern' style of creating arguments which is
// // more verbose, but allows easier editing, and at times more advanced options, or the possibility
// // to generate arguments dynamically.
mod clients;
mod errors;
mod http_server;
mod tealer_actor;
use chrono::Duration;
use clap::{Arg, ArgMatches};
use iqiper_tealer::{
    IqiperTealerActor, IqiperTealerActorAuthorizationCodeGrant, IqiperTealerActorPasswordGrant, IqiperTealerActorClientCredentialGrant
};
use log::{debug, info, warn};
use iqiper_tealer::oidc::OAuth2TokenResponse;
use std::path::Path;
use std::sync::mpsc;
use std::thread;
use tealer_actor::IqiperTealerActorCli;

fn setup_logger(cli: &ArgMatches) {
    match cli.is_present("quiet") {
        true => env_logger::Builder::new()
            .filter_level(log::LevelFilter::Off)
            .init(),
        false => env_logger::Builder::new()
            .filter(Some("actix_server"), log::LevelFilter::Warn)
            .filter(Some("actix_web"), log::LevelFilter::Warn)
            .parse_filters(
                std::env::var("RUST_LOG")
                    .unwrap_or(String::from("info"))
                    .as_str(),
            )
            .init(),
    };
}

fn get_default_storage_path() -> String {
    let mut storage_path = std::env::current_dir().expect("Can't get current directory path");
    storage_path.push(std::path::Path::new(".iqiper-token-storage.json"));
    storage_path.to_string_lossy().to_owned().to_string()
}

fn cli(storage_path: &String) -> ArgMatches<'_> {
    clap::App::new("iqiper-oidc-cli")
        .version("0.1.1")
        .author("Francis (GrandChaman) Le Roy <francis.leroy@protonmail.ch>")
        .about("Interface with an IdP for testing purposes")
        .arg(
            Arg::with_name("scopes")
                .short("s")
                .long("scopes")
                .value_name("scope")
                .help("The scopes to ask for, space separated")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("host")
                .short("h")
                .long("host")
                .value_name("hostname")
                .help("The host to bind to")
                .default_value("127.0.0.1")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("port")
                .help("The port to bind to")
                .default_value("4567")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("client-id")
                .short("i")
                .long("client-id")
                .value_name("client id")
                .help("The client id to connect to")
                .required(true)
                .takes_value(true),
		)
        .arg(
            Arg::with_name("client-secret")
                .short("S")
                .long("client-secret")
                .value_name("client secret")
                .help("The client secretto connect with")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("storage")
                .short("f")
                .long("storage")
                .value_name("storage")
                .help("The storage path")
                .required(false)
                .default_value(storage_path.as_str())
                .takes_value(true),
        )
        .arg(
            Arg::with_name("insecure")
                .short("k")
                .long("insecure")
                .help("Disable TLS hostname verification")
                .required(false)
                .takes_value(false),
        )
        .arg(
            Arg::with_name("display")
                .short("d")
                .long("display")
                .help("Print the access token to the standard output")
                .required(false)
                .takes_value(false),
        )
        .arg(
            Arg::with_name("root-cert")
                .short("r")
                .long("root-cert")
                .help("Specify the path of the root certificate to use")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("authorization-code-flow")
                .short("A")
                .long("auth-code-flow")
                .help("Use the auth code flow")
                .required_unless("flow")
				// .conflicts_with_all(&["password-flow", "client-flow"])
				.group("flow")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("password-flow")
                .short("D")
                .long("password-flow")
                .help("Use the password flow")
                .required_unless("flow")
                .requires_all(&["user", "password"])
				// .conflicts_with_all(&["authorization-code-flow", "client-flow"])
				.group("flow")
                .takes_value(false),
		)
        .arg(
            Arg::with_name("client-flow")
                .short("C")
                .long("client-flow")
                .help("Use the client flow")
                .required_unless("flow")
				// .conflicts_with_all(&["authorization-code-flow", "password-flow"])
                .requires("client-secret")
				.group("flow")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("password")
                .short("P")
                .long("password")
                .help("Specify password for the password flow")
                .conflicts_with("authorization-code-flow")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("user")
                .short("U")
                .long("user")
                .help("Specify user for the password flow")
                .conflicts_with("authorization-code-flow")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("quiet")
                .short("q")
                .long("quiet")
                .help("Disable outputing info/warn/error")
                .takes_value(false),
        )
        .arg(
            Arg::with_name("idp-discovery-url")
                .help("The URI on which to open the browser")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("token-url")
                .help("The URI on which to send the authorization code")
                .required(true)
                .index(2),
        )
        .get_matches()
}

async fn get_new_token_routine_auth_code_flow(
    cli: &ArgMatches<'_>,
    tealer_actor: &IqiperTealerActorCli,
    scopes: &Vec<iqiper_tealer::oidc::Scope>,
) -> iqiper_tealer::IqiperTealerToken {
    let (tx_srv, rx_srv) = mpsc::channel();
    let (tx_sd, rx_sd) = mpsc::channel();
    let host = String::from(cli.value_of("host").unwrap());
    let port = String::from(cli.value_of("port").unwrap());
    debug!("Starting the callback server");
    thread::spawn(move || {
        let _ = http_server::run_web_server(tx_srv, tx_sd, host, port);
    });
    debug!("Waiting for server handle");
    let srv = rx_srv.recv().unwrap();
    debug!("Received handle, sending request to IdP");
    let (auth_url, state) =
        clients::get_authorize_url(&cli, tealer_actor.tealer().oidc_client(), scopes)
            .expect("The authorize url");
    opener::open(auth_url.to_string()).expect("To open the default browser");
    let query_param = rx_sd.recv().unwrap();
    let token = tealer_actor
        .get_token_via_code(
            &iqiper_tealer::oidc::AuthorizationCode::new(query_param.code),
            Some(&state),
		)
		.await
        .expect("The token");
    debug!("Stopping callback server");
    srv.stop(true).await;
    token
}

async fn get_new_token_routine(
    cli: &ArgMatches<'_>,
    tealer_actor: &IqiperTealerActorCli,
    scopes: &Vec<iqiper_tealer::oidc::Scope>,
) -> iqiper_tealer::IqiperTealerToken {
	if cli.is_present("authorization-code-flow") {
		get_new_token_routine_auth_code_flow(cli, tealer_actor, scopes).await
	}
	else if cli.is_present("password-flow")
	{
		tealer_actor
		.get_token_via_password(
			&iqiper_tealer::oidc::ResourceOwnerUsername::new(
				cli.value_of("user").unwrap().to_string(),
			),
			&iqiper_tealer::oidc::ResourceOwnerPassword::new(
				cli.value_of("password").unwrap().to_string(),
			),
			scopes,
		)
		.await
		.expect("The password grant failed")
	}
	else if cli.is_present("client-flow")
	{
		tealer_actor
		.get_token_via_client_credential(scopes)
		.await
		.expect("The client grant failed")
	}
	else
	{
		unreachable!();
	}
}

async fn add_new_token_to_tealer(
    cli: &ArgMatches<'_>,
    tealer_actor: &mut IqiperTealerActorCli,
    scopes: &Vec<iqiper_tealer::oidc::Scope>) -> iqiper_tealer::IqiperTealerToken {
    let new_token = get_new_token_routine(cli, tealer_actor, scopes).await;
    tealer_actor.tealer_mut().add_token(
        new_token.clone(),
        scopes.clone(),
        cli.value_of("user").map(|x| x.to_string()),
    );
    new_token
}

#[tokio::main]
async fn main() {
    openssl_probe::init_ssl_cert_env_vars();
    let default_storage_path = get_default_storage_path();
    let cli = cli(&default_storage_path);
    setup_logger(&cli);
    let redirect_uri = iqiper_tealer::oidc::RedirectUrl::new(format!(
        "http://{}:{}/auth_code",
        cli.value_of("host").unwrap(),
        cli.value_of("port").unwrap()
    ))
	.expect("The built redirect URI");
    let mut tealer = iqiper_tealer::IqiperTealer::new_authorization_code_grant(
		clients::reqwest_client_build(&cli).expect("The reqwest client"),
        iqiper_tealer::oidc::IssuerUrl::new(cli.value_of("idp-discovery-url").unwrap().to_string())
            .expect("The issuer URL"),
        redirect_uri.clone(),
        iqiper_tealer::oidc::ClientId::new(cli.value_of("client-id").unwrap().to_string()),
       	cli.value_of("client-secret").map(|x| iqiper_tealer::oidc::ClientSecret::new(x.to_string())),
	)
	.await
    .expect("The tealer");
    let scopes: Vec<iqiper_tealer::oidc::Scope> = match cli.value_of("scopes") {
        Some(x) =>
            x.split_whitespace()
                .map(|x| iqiper_tealer::oidc::Scope::new(String::from(x)))
                .collect(),
        None => vec![],
    };
    let storage_path = Path::new(cli.value_of("storage").unwrap());
    if tealer.store_from_file(storage_path).is_err() {
        warn!("Empty store file!");
    }
    let mut tealer_actor = IqiperTealerActorCli::new(
        tealer,
        redirect_uri.clone(),
        url::Url::parse(cli.value_of("token-url").unwrap()).expect("A valid token url"),
    );
    let token = match tealer_actor.tealer().get_token(&scopes, None) {
        Some(token) => match token.check_exp_with_leeway(Duration::seconds(5)) {
            true => match tealer_actor.refresh_token(&token).await {
                Ok(token) => tealer_actor
                    .tealer_mut()
                    .add_token(token.clone(), scopes, None)
                    .unwrap_or(token),
                Err(_x) => add_new_token_to_tealer(&cli, &mut tealer_actor, &scopes).await,
            },
            false => {
                info!("Valid token found");
                token.clone()
            }
        },
        None => add_new_token_to_tealer(&cli, &mut tealer_actor, &scopes).await,
    };
    if cli.is_present("display") {
        println!("{}", token.token().access_token().secret());
    }
    tealer_actor
        .tealer()
        .store_to_file(&storage_path)
        .expect("Can't write token to storage");
}
