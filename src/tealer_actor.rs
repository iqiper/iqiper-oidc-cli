use async_trait::async_trait;
use iqiper_tealer::oidc::OAuth2TokenResponse;
use iqiper_tealer::*;
use log::{error, info, warn};

pub struct IqiperTealerActorCli {
    tealer: IqiperTealer,
    redirect_url: iqiper_tealer::oidc::RedirectUrl,
    token_url: url::Url,
}

impl IqiperTealerActorCli {
    pub fn new(
        tealer: IqiperTealer,
        redirect_url: iqiper_tealer::oidc::RedirectUrl,
        token_url: url::Url,
    ) -> Self {
        IqiperTealerActorCli {
            tealer,
            redirect_url,
            token_url,
        }
    }
}

#[async_trait]
impl IqiperTealerActor for IqiperTealerActorCli {
    fn tealer(&self) -> &IqiperTealer {
        &self.tealer
    }

    fn tealer_mut(&mut self) -> &mut IqiperTealer {
        &mut self.tealer
    }

    fn get_grant_type(&self) -> &IqiperTealerGrant {
        &IqiperTealerGrant::AuthorizationCodeGrant
    }

    async fn refresh_token(
        &self,
        token: &IqiperTealerToken,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        if token.token().refresh_token().is_none() {
            return Err(IqiperTealerError::NoRefreshToken);
        }
        info!("Refreshing tokens...");
        let mut url = self.token_url.clone();
        url.query_pairs_mut()
            .clear()
            .append_pair("grant_type", "refresh_token")
            .append_pair(
                "refresh_token",
                token.token().refresh_token().unwrap().secret(),
            );
        let token: serde_json::Value = self
            .tealer
            .reqwest_client()
            .get(url)
            .send()
            .await?
            .json()
            .await?;
        match serde_json::from_value::<iqiper_tealer::oidc::core::CoreTokenResponse>(token.clone())
        {
            Ok(x) => {
                info!("Done!");
                Ok(IqiperTealerToken::new(x))
            }
            Err(x) => {
                warn!(
                    "{}",
                    format!(
                        "Error serializing : {:#?} into a token. Got error : {:#?}",
                        token, x
                    )
                );
                Err(IqiperTealerError::Serde(x))
            }
        }
    }
}

#[async_trait]
impl IqiperTealerActorAuthorizationCodeGrant for IqiperTealerActorCli {
    async fn get_token_via_code(
        &self,
        code: &iqiper_tealer::oidc::AuthorizationCode,
        state: Option<&iqiper_tealer::oidc::CsrfToken>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let mut url = self.token_url.clone();
        info!("Getting new tokens...");
        url.query_pairs_mut()
            .clear()
            .append_pair("grant_type", "authorization_code")
            .append_pair("redirect_uri", &self.redirect_url)
            .append_pair("code", code.secret().as_str());
        if state.is_some() {
            url.query_pairs_mut()
                .append_pair("state", state.unwrap().secret().as_str());
        }
        let token: serde_json::Value = self
            .tealer
            .reqwest_client()
            .get(url)
            .send()
            .await?
            .json()
            .await?;
        match serde_json::from_value::<iqiper_tealer::oidc::core::CoreTokenResponse>(token.clone())
        {
            Ok(x) => {
                info!("Done!");
                Ok(IqiperTealerToken::new(x))
            }
            Err(x) => {
                error!(
                    "{}",
                    format!(
                        "Error serializing : {:#?} into a token. Got error : {:#?}",
                        token, x
                    )
                );
                Err(IqiperTealerError::Serde(x))
            }
        }
    }
}

#[async_trait]
impl IqiperTealerActorPasswordGrant for IqiperTealerActorCli {
    async fn get_token_via_password(
        &self,
        user: &iqiper_tealer::oidc::ResourceOwnerUsername,
        password: &iqiper_tealer::oidc::ResourceOwnerPassword,
        scopes: &Vec<iqiper_tealer::oidc::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let mut url = self.token_url.clone();
        info!("Getting new tokens...");
        url.query_pairs_mut()
            .clear()
            .append_pair("grant_type", "password")
            .append_pair("username", user.as_str())
            .append_pair("password", password.secret().as_str())
            .append_pair(
                "scope",
                scopes
                    .into_iter()
                    .map(|x| x.to_string())
                    .collect::<Vec<String>>()
                    .join(" ")
                    .as_str(),
            );
        let token: serde_json::Value = self
            .tealer
            .reqwest_client()
            .get(url)
            .send()
            .await?
            .json()
            .await?;
        match serde_json::from_value::<iqiper_tealer::oidc::core::CoreTokenResponse>(token.clone())
        {
            Ok(x) => {
                info!("Done!");
                Ok(IqiperTealerToken::new(x))
            }
            Err(x) => {
                error!(
                    "{}",
                    format!(
                        "Error serializing : {:#?} into a token. Got error : {:#?}",
                        token, x
                    )
                );
                Err(IqiperTealerError::Serde(x))
            }
        }
	}
    async fn try_get_token_via_password(
        &mut self,
        username: &iqiper_tealer::oidc::ResourceOwnerUsername,
        password: &iqiper_tealer::oidc::ResourceOwnerPassword,
        scopes: &Vec<iqiper_tealer::oidc::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let tealer = self.tealer();
        let token = match tealer.get_token(scopes, Some(username.to_string())) {
            Some(token) => match token.check_exp_with_leeway(chrono::Duration::seconds(1)) { //TODO Better leeway
                true => match self.refresh_token(&token).await {
                    Ok(token) => Ok(token),
                    Err(_x) => self.get_token_via_password(username, password, scopes).await,
                },
                false => Ok(token.clone()),
            },
            None => self.get_token_via_password(username, password, scopes).await,
        }?;
        drop(tealer);
        self.tealer_mut()
            .add_token(token.clone(), scopes.clone(), Some(username.to_string()));
        Ok(token)
    }
}

#[async_trait]
impl IqiperTealerActorClientCredentialGrant for IqiperTealerActorCli {
    async fn get_token_via_client_credential(
        &self,
        scopes: &Vec<iqiper_tealer::oidc::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        info!("Getting new tokens...");
        let mut req = self.tealer.oidc_client().exchange_client_credentials();
        for scope in scopes.iter().cloned() {
            req = req.add_scope(scope);
        }
        let reqwest_ref = self.tealer.reqwest_client().clone();
        Ok(req
            .request_async(
                move |req| async move { openid_reqwest_client_async(reqwest_ref, req).await },
            )
            .await
            .map(|t| IqiperTealerToken::new(t))?)
	}
	
    async fn try_get_token_via_client_credential(
        &mut self,
        scopes: &Vec<iqiper_tealer::oidc::Scope>,
    ) -> Result<IqiperTealerToken, IqiperTealerError> {
        let tealer = self.tealer();
        let token = match tealer.get_token(scopes, None) {
            Some(token) => match token.check_exp_with_leeway(chrono::Duration::seconds(1)) { //TODO Better leeway
                true => match self.refresh_token(&token).await {
                    Ok(token) => Ok(token),
                    Err(_x) => self.get_token_via_client_credential(scopes).await,
                },
                false => Ok(token.clone()),
            },
            None => self.get_token_via_client_credential(scopes).await,
        }?;
        drop(tealer);
        self.tealer_mut().add_token(token.clone(), scopes.clone(), None);
        Ok(token)
    }
}
