use crate::errors::IqiperOIDCCliError;
use iqiper_tealer::oidc::core::{CoreAuthenticationFlow, CoreClient};
use iqiper_tealer::oidc::{CsrfToken, Nonce};
use std::fs::File;
use std::io::Read;

pub fn reqwest_client_build(
    conf: &clap::ArgMatches<'_>,
) -> Result<reqwest::Client, IqiperOIDCCliError> {
    let mut builder = reqwest::Client::builder().redirect(reqwest::redirect::Policy::none());
    if conf.is_present("insecure") {
        builder = builder.danger_accept_invalid_certs(true);
    }
    if conf.is_present("root-cert") {
        let mut buf = Vec::new();
        File::open(conf.value_of("root-cert").unwrap())?.read_to_end(&mut buf)?;
        let cert = reqwest::Certificate::from_pem(&buf)?;
        builder = builder.add_root_certificate(cert);
    }
    Ok(builder.build()?)
}

pub fn get_authorize_url(
    _conf: &clap::ArgMatches<'_>,
    client: &CoreClient,
    scopes: &Vec<iqiper_tealer::oidc::Scope>,
) -> Result<(iqiper_tealer::oidc::url::Url, CsrfToken), IqiperOIDCCliError> {
    let mut auth_req = client.authorize_url(
        CoreAuthenticationFlow::AuthorizationCode,
        CsrfToken::new_random,
        Nonce::new_random,
    );
    for scope in scopes.into_iter().cloned() {
        auth_req = auth_req.add_scope(scope);
    }
    let (auth_url, csrf_token, _nonce) = auth_req.url();
    Ok((auth_url, csrf_token))
}
