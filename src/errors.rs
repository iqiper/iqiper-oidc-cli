use std::fmt;

#[derive(Debug)]
pub enum IqiperOIDCCliError {
    IO(std::io::Error),
    CLI(clap::Error),
    URL(url::ParseError),
    Serde(serde_json::Error),
    Reqwest(reqwest::Error),
    OICDiscovery(iqiper_tealer::oidc::DiscoveryError<reqwest::Error>),
    TokenRequest(
        iqiper_tealer::oidc::RequestTokenError<
            reqwest::Error,
            iqiper_tealer::oidc::StandardErrorResponse<
                iqiper_tealer::oidc::core::CoreErrorResponseType,
            >,
        >,
    ),
    Tealer(iqiper_tealer::IqiperTealerError),
}

impl fmt::Display for IqiperOIDCCliError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            IqiperOIDCCliError::IO(io_err) => write!(f, "{}", io_err),
            IqiperOIDCCliError::Reqwest(err) => write!(f, "{}", err),
            IqiperOIDCCliError::CLI(err) => write!(f, "{}", err),
            IqiperOIDCCliError::URL(err) => write!(f, "{}", err),
            IqiperOIDCCliError::Serde(err) => write!(f, "{}", err),
            IqiperOIDCCliError::OICDiscovery(err) => write!(f, "{}", err),
            IqiperOIDCCliError::TokenRequest(err) => write!(f, "{}", err),
            IqiperOIDCCliError::Tealer(err) => write!(f, "{}", err),
        }
    }
}

// impl IqiperOIDCCliError {
//     pub fn from_url(msg: String) -> Self {
//         IqiperOIDCCliError::URL(msg)
//     }
// }

impl From<std::io::Error> for IqiperOIDCCliError {
    fn from(err: std::io::Error) -> Self {
        IqiperOIDCCliError::IO(err)
    }
}

impl From<reqwest::Error> for IqiperOIDCCliError {
    fn from(err: reqwest::Error) -> Self {
        IqiperOIDCCliError::Reqwest(err)
    }
}

impl From<clap::Error> for IqiperOIDCCliError {
    fn from(err: clap::Error) -> Self {
        IqiperOIDCCliError::CLI(err)
    }
}

impl From<serde_json::Error> for IqiperOIDCCliError {
    fn from(err: serde_json::Error) -> Self {
        IqiperOIDCCliError::Serde(err)
    }
}

impl From<url::ParseError> for IqiperOIDCCliError {
    fn from(err: url::ParseError) -> Self {
        IqiperOIDCCliError::URL(err)
    }
}

impl From<iqiper_tealer::oidc::DiscoveryError<reqwest::Error>> for IqiperOIDCCliError {
    fn from(err: iqiper_tealer::oidc::DiscoveryError<reqwest::Error>) -> Self {
        IqiperOIDCCliError::OICDiscovery(err)
    }
}

impl From<iqiper_tealer::IqiperTealerError> for IqiperOIDCCliError {
    fn from(err: iqiper_tealer::IqiperTealerError) -> Self {
        IqiperOIDCCliError::Tealer(err)
    }
}

impl
    From<
        iqiper_tealer::oidc::RequestTokenError<
            reqwest::Error,
            iqiper_tealer::oidc::StandardErrorResponse<
                iqiper_tealer::oidc::core::CoreErrorResponseType,
            >,
        >,
    > for IqiperOIDCCliError
{
    fn from(
        err: iqiper_tealer::oidc::RequestTokenError<
            reqwest::Error,
            iqiper_tealer::oidc::StandardErrorResponse<
                iqiper_tealer::oidc::core::CoreErrorResponseType,
            >,
        >,
    ) -> Self {
        IqiperOIDCCliError::TokenRequest(err)
    }
}
